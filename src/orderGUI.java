import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;


public class orderGUI {
    private JPanel root;
    private JLabel TitleLabel;
    private JLabel OrderLabel;
    private JButton VeggieDeliteButton;
    private JButton eggButton;
    private JTextArea OrderedItems;
    private JLabel allPriceLabel;
    private JButton AbocadoButton;
    private JButton TeriyakiButton;
    private JButton BLTButton;
    private JButton PizaItalianaButton;
    private JButton checkOutButton;
    private JRadioButton salad;
    private JRadioButton soup;
    private JButton DrinkButton;
    private JRadioButton poteto;
    private JButton Cancel;

    private int allprice=0;
    private int price=0;

    private String food;
    private String[] drink={"AppleJuice","Tea","Coffee"};

    public void side(){
        if(salad.isSelected() || soup.isSelected() ||poteto.isSelected()) {
            food += "    Side menu: ";

            if(poteto.isSelected()){
                price += 120;
                food += "  poteto, ";
            }
            if (salad.isSelected()) {
                    price += 110;
                    food += " salad, ";
                }
            if (soup.isSelected()) {
                price += 100;
                food += "  soup, ";
            }
        }
    }
    public void drink(){
        int choice_drink = JOptionPane.showOptionDialog(
                null,
                "Would you like to order drink?",
                "drink order",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                drink,
                drink[1]
        );
        if(drink[choice_drink]=="AppleJuice"){
            price=290;

        }else if(drink[choice_drink]=="Tea"){
            price=240;

        }else if(Objects.equals(drink[choice_drink], "Coffee")){
            price=260;

        }
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "your order information\n"
                        +"Menu:"+ drink[choice_drink] +"\n"
                        +"Price:"+ price +"\n",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            String currentText = OrderedItems.getText();
            OrderedItems.setText(currentText + drink[choice_drink] +"     "+price+" yen\n");
            JOptionPane.showMessageDialog(null,"Order for "+ drink[choice_drink] +" received.");
            allprice += price;
            allPriceLabel.setText("Total "+allprice + " yen");
        }
    }
    public void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "your order information\n"
                        +"Menu:"+ food +"\n"
                        +"Price:"+ price +"\n",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            String currentText = OrderedItems.getText();
            OrderedItems.setText(currentText + food +"     "+price+" yen\n");
            JOptionPane.showMessageDialog(null,"Order for "+ food +" received.");
            allprice += price;
            allPriceLabel.setText("Total          "+allprice + " yen");
            salad.setSelected(false);
            soup.setSelected(false);
            poteto.setSelected(false);
        }
    }
    public void CheckOut(){

        if(allprice==0){
            JOptionPane.showMessageDialog(
                    null,
                    "Please select menu."
            );
            return;
        }
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                " Would you like to check out?\n",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for buying!\n"+"total price is "+allprice+"yen\n"
            );
            OrderedItems.setText(null);
            allprice=0;
            allPriceLabel.setText("Total          "+allprice+" yen");


        }
    }
public orderGUI() {

    VeggieDeliteButton.setIcon(new ImageIcon(
            this.getClass().getResource("Veggie_Delite.jpg")
    ));
    VeggieDeliteButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            price=430;
            food="Veggie Delite";
            side();
            order(food,price);

        }
    });
    eggButton.setIcon(new ImageIcon(
            this.getClass().getResource("egg.jpg")
    ));
    eggButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            price=490;
            food="Egg Sandwich";
            side();
            order(food,price);
        }
    });
    PizaItalianaButton.setIcon(new ImageIcon(
            this.getClass().getResource("bacon_piza.jpg")
    ));
    PizaItalianaButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            price=590;
            food="Pizza Bacon Italiana";
            side();
            order(food,price);
        }
    });
    BLTButton.setIcon(new ImageIcon(
            this.getClass().getResource("BLT.jpg")
    ));
    BLTButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            price=550;
            food="BLT";
            side();
            order(food,price);
        }
    });
    AbocadoButton.setIcon(new ImageIcon(
            this.getClass().getResource("AvocadoVeggie.jpg")
    ));
    AbocadoButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            price=490;
            food="Avocado & Veggie";
            side();
            order(food,price);
        }
    });
    TeriyakiButton.setIcon(new ImageIcon(
            this.getClass().getResource("teriyaki_chicken.jpg")
    ));
    TeriyakiButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            price=550;
            food="Teriyaki Chicken";
            side();
            order(food,price);
        }
    });
    checkOutButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            CheckOut();
        }
    });
    DrinkButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            drink();

        }
    });
    Cancel.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            OrderedItems.setText(null);
            allprice=0;
            allPriceLabel.setText("Total          "+allprice+" yen");
        }
    });
}

    public static void main(String[] args) {
        JFrame frame = new JFrame("orderGUI");
        frame.setContentPane(new orderGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
